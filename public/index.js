document.addEventListener("DOMContentLoaded", function () {
    function updatePrice() {
        if (price === 50) {
            result.value = count * price;
        }
        if (price === 100) {
            if (radios[1].checked) {
                result.value = count * (price + 5);
            } else {
                result.value = count * price;
            }
        }
        if (price === 150) {
            if (checkboxes[0].checked && checkboxes[1].checked) {
                result.value = count * (price + 40);
            } else {
                if (checkboxes[0].checked || checkboxes[1].checked) {
                    result.value = count * (price + 20);
                } else {
                    result.value = count * price;
                }
            }
        }
    }
    const countInput = document.getElementById("count");
    const result = document.getElementById("result");
    const product = document.getElementById("product");
    const checkboxBoxes = document.getElementsByClassName("checkbox");
    const radioBoxes = document.getElementsByClassName("radio");
    const checkboxes = document.getElementsByClassName("inpc");
    const radios = document.getElementsByClassName("inpr");
    let price = 50;
    let count = 0;
    updatePrice();
    countInput.addEventListener("change", function (event) {
        count = event.target.value;
        updatePrice();
    });
    [...checkboxes].forEach(function (el) {
        el.addEventListener("change", function () {
            updatePrice();
        });
    });
    [...radios].forEach(function (el) {
        el.addEventListener("change", function () {
            updatePrice();
        });
    });
    product.addEventListener("change", function (event) {
        if (event.target.value === "water") {
            [...checkboxBoxes].forEach(function (el) {
                el.classList = "checkbox hidden";
            });
            [...radioBoxes].forEach(function (el) {
                el.classList = "radio hidden";
            });
            price = 50;
        }
        if (event.target.value === "waffles") {
            [...checkboxBoxes].forEach(function (el) {
                el.classList = "checkbox hidden";
            });
            [...radioBoxes].forEach(function (el) {
                el.classList = "radio";
            });
            price = 100;
        }
        if (event.target.value === "ice cream") {
            [...checkboxBoxes].forEach(function (el) {
                el.classList = "checkbox";
            });
            [...radioBoxes].forEach(function (el) {
                el.classList = "radio hidden";
            });
            price = 150;
        }
        updatePrice();
    });
});

